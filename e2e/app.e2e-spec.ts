import { DraftAreaPage } from './app.po';

describe('draft-area App', () => {
  let page: DraftAreaPage;

  beforeEach(() => {
    page = new DraftAreaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
