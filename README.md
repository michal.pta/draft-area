# ng-draft-area

An angular textarea component which automatically saves a draft of user's input.

https://gitlab.com/michal.pta/draft-area.git   
https://www.npmjs.com/package/ng-draft-area

## Demo page
http://michal.pta.gitlab.io/draft-area/

## How to use

### 1. Install package
```shell
npm install ng-draft-area --save
```

### 2. Import module
```typescript
import { DraftAreaModule } from 'ng-draft-area';

@NgModule({ 
  ...
  imports: [ 
    ...
    DraftAreaModule
    ...
  ], 
  ...
})
```

### 3. Use in your template
```html
<draft-area [(value)]="model" [key]="uniqueKey"></draft-area>
```

## How it works
**ng-draft-area** works the same way as standard textarea.

The only difference happens behind the scenes. In the background it automatically saves every user's input in the `localstorage` of the user's browser (under the unique customizable key). In other words it creates a backup of what the user is entering into the textarea.

Then, whenever there's a difference between the value saved in the `localstorage` and the value passed to the `value` property, the component presents both versions on a single screen, and allows to easily compare / restore / discard the changes.

## Properties

| Name | Type | Default | Description |
|------|------|---------|-------------|
| value | string | "" | Value of the textarea. Any string property of your model that you want to edit via the draft-area can be bound to this input. |
| key | string | "comment" | Unique key to be used for storing the information in localstorage. |
| label | string | "Comment" | Label to be displayed above the textarea. |
| rows | number | 10 | Number of rows visible in the textarea. Value of the rows attribute of the textarea. |
