import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DraftAreaModule } from './draft-area';
import { DraftAreaModule as NpmDraftAreaModule } from 'ng-draft-area';

import { environment } from 'environments/environment';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    environment.production ? NpmDraftAreaModule : DraftAreaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
